// Aboutabl CS 450 Extra Credit: Santa and the Reindeers
// Author: Cole Inman
// Date: 11/14/2015

#include "mrsclaus.h"

int main()
{
	int shmid;
	key_t shmkey;
	int shmflg;
	shared_data *shared;

	shmkey = SHMKEY;
	shmflg = IPC_CREAT | S_IRUSR | S_IWUSR;
	shmid = shmget( shmkey, SHMEM_SIZE, shmflg);
	if (shmid != -1)
	{
		printf("Santa process started successfully with shared memory id: %d\n", shmid);
	}

	shared = (shared_data *) shmat(shmid, NULL, 0);
	srandom(getpid());//random number generator

	while (true)
	{
		printf("Santa is going to sleep till all Reindeer arrive or Elves need help\n");
		sem_wait(&shared->santa_wake);

		// Reindeer wake santa up
		if (shared->deer_count == NUM_DEER)
		{
			printf("Santa: I woke up by a Reindeer\n");
			printf("Santa: harnessing all reindeers to the sleigh\n");
			sleep(3);

			printf("Santa: HO HO HO, Let's distribute toys\n");
			for (int i = 0; i < 4; i++)
				sem_post(&shared->hohoho);

			int fly = (random() % 5) + 1;
			printf("Santa: flying sleigh for %d seconds\n", fly);
			sleep(fly);

			printf("Santa: back after Christmas\n");
			for (int i = 0; i < 4; i++)
				sem_post(&shared->christmas);

			for (int i = 0; i < NUM_DEER; i++)
				sem_wait(&shared->back);

			--shared->years;
			printf("Santa: Thank You reindeers!\n");
			for (int i = 0; i < 5; i++)
					sem_post(&shared->free);
			sem_post(&shared->santa_out);
		}

		// Elves wake up santa
		else //if (shared->elves_waiting_on_santa >= 3 || (shared->elves_working < 3 && shared->elves_waiting_on_santa > 0))
		{
			printf("Santa: I woke up by an Elf\n");
			for (int i = 0; i < 3; i++)
			{
					printf("Santa: Let me hear your problem, elf\n");
					sem_post(&shared->permission);

					sem_wait(&shared->speaking);
					printf("Santa: Here is my solution, elf...\n");
					sem_post(&shared->solution);
					sem_post(&shared->elf_sesh);
			}
			sem_wait(&shared->go_sleep);
			sem_post(&shared->santa_out);
		}
	}
}
