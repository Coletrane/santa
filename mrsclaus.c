// Aboutabl CS 450 Extra Credit: Santa and the Reindeers
// Author: Cole Inman
// Date: 11/14/2015

#include "mrsclaus.h"

int main(int argc, char *argv[])
{
	int shmid;
	key_t shmkey;
	int shmflg;
	shared_data *shared;

	shmkey = SHMKEY;
	shmflg = IPC_CREAT | S_IRUSR | S_IWUSR;
	shmid = shmget( shmkey, SHMEM_SIZE, shmflg);
	if (shmid != -1)
	{
		printf("Mrs. Claus process started successfully with shared memory id: %d\n", shmid);
	}

	shared = (shared_data *) shmat(shmid, NULL, 0);

	// Init command line args
	shared->years = atoi(argv[1]) - 1;
	shared->toys = atoi(argv[2]);
	shared->fail = atoi(argv[3]);
	shared->deer_count = 0;
	shared->init_deer_count = 0;
	shared->elves_working = 25;
	shared->elves_front_desk = 0;
	shared->elf_workshop = 0;

	// Init semaphores
	sem_init(&shared->tropics, 1, (unsigned int)4);
	sem_init(&shared->santa_wake, 1, 0);
	sem_init(&shared->santa, 1, 0);
	sem_init(&shared->hohoho, 1, 0);
	sem_init(&shared->christmas, 1, 0);
	sem_init(&shared->free, 1, 0);
	sem_init(&shared->back, 1, 0);
	sem_init(&shared->hut, 1, 0);
	sem_init(&shared->deer, 1, 1);
	sem_init(&shared->seat, 1, 3);
	sem_init(&shared->solution, 1, 0);
	sem_init(&shared->meeting, 1, 0);
	sem_init(&shared->problem,1,1);
	sem_init(&shared->permission,1, 0);
	sem_init(&shared->speaking, 1, 0);
	sem_init(&shared->go_sleep, 1, 0);
	sem_init(&shared->mutex, 1, 1);
	sem_init(&shared->elf_sesh, 1, 1);
	sem_init(&shared->santa_out, 1, 1);

	const char deer[20] = "reindeer";
	const char elf[20] = "elf";
	const char santa[20] = "santa";

	char deerid[20];
	char elfid[20];
	srandom(getpid());


	for (int i = 1; i < NUM_ELVES + 1; i++)
	{
		pid_t pid = fork();
		if (pid == 0)
		{
			snprintf(elfid, 20, "%d", i);
			wait(4);
			execl(elf, elfid, argv[2], argv[3]);
		}
	}


	for (int i = 1; i < NUM_DEER + 1; i++)
	{
		pid_t p = fork();
		if (p == 0)
		{
			snprintf(deerid, 20, "%d", i);
			wait(4);
			execl(deer, deerid, argv[1]);
		}
	}

	pid_t pd = fork();
	if (pd == 0)
		execl(santa, NULL);

	while (1)
	{
		sem_wait(&shared->mutex);
		if (shared->elves_working == 0 && shared->years == 0)
		{
			printf("Mrs. Claus is killing all processes and destroying shared memory, Merry Christmas!\n");

		}
		sem_post(&shared->mutex);
	}
}



