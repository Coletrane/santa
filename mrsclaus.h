// Aboutabl CS 450 Extra Credit: Santa and the Reindeers
// Author: Cole Inman
// Date: 11/14/2015

#ifndef mrsclaus_h
#define mrsclaus_h

#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <stdbool.h>

#define SHMKEY 987546
#define SHMEM_SIZE sizeof(shared_data)

#define NUM_DEER 4
#define NUM_SEATS 3
#define NUM_ELVES 25

typedef struct {
	sem_t tropics;
	sem_t santa_wake;
	sem_t hut;
	sem_t deer;
	sem_t santa;
	sem_t hohoho;
	sem_t back;
	sem_t christmas;
	sem_t free;
	sem_t seat;
	sem_t meeting;
	sem_t speaking;
	sem_t problem;
	sem_t solution;
	sem_t permission;
	sem_t go_sleep;
	sem_t santa_out;
	sem_t mutex;
	sem_t elf_sesh;
	int years;
	int toys;
	int fail;
	int init_deer_count;
	int deer_count;
	int elves_working;
	int elf_workshop;
	int elves_front_desk;
	int kill;
}shared_data;
#endif
