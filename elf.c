
#include "mrsclaus.h"

int main(int argc, char *argv[])
{
	int id = atoi(argv[0]);
	int num_toys = atoi(argv[1]);
	int fail_rate = atoi(argv[2]);
	int anti_deadlock = 0;
	int toy_number = 1;
	shared_data *shared;
	int shmflg, shmid, shmkey;

	shmkey = SHMKEY;
	shmflg = IPC_CREAT | S_IRUSR | S_IWUSR;
	shmid = shmget( shmkey, SHMEM_SIZE, shmflg);
	if (shmid != -1)
	{
		printf("Elf process id: %d started with shared memory id: %d\n", id, shmid);
	}

	shared = (shared_data *) shmat(shmid, NULL, 0);
	srandom(getpid());

	while (num_toys > 0)
	{
		int make = (random() % 4) + 1;
		printf("Elf: %d making toy #%d for %d seconds\n", id, toy_number, make);
		sleep(make);
		int f = (random() % 100) + 1;

		// Manufacturing toy fails
		if (f <= fail_rate)
		{
			printf("Elf: %d problem encountered!!\n", id);
			sem_wait(&shared->seat);
			printf("Elf: %d found a seat\n", id);

			sem_wait(&shared->mutex);
			++shared->elves_front_desk;
			printf("Elf: %d Elves at front desk: %d\n", id, shared->elves_front_desk);
			sem_post(&shared->mutex);

			sem_wait(&shared->mutex);
			if (shared->elves_working > 3)
			{
				// Elves that wait for santa to be woken up
				if (shared->elves_front_desk < 3)
				{
					sem_wait(&shared->meeting);
				}
				// Elf that wakes up santa
				else
				{
					for (int i = 0; i < 3; i++)
						--shared->elves_front_desk;
					sem_wait(&shared->santa_out);
					printf("Elf: %d waking up Santa\n", id);
					sem_post(&shared->santa_wake);

					printf("Elf: %d prepare to meet Santa!\n", id);
					sem_post(&shared->meeting);
					sem_post(&shared->meeting);
				}
			}
			sem_post(&shared->mutex);
			sem_wait(&shared->mutex);
			if (shared->elves_working >= 3)
			{
				sem_wait(&shared->santa_out);
				printf("Elf: %d waking up Santa\n", id);
				sem_post(&shared->santa_wake);
			}
			sem_post(&shared->mutex);

			sem_wait(&shared->mutex);
			++shared->elf_workshop;
			sem_post(&shared->mutex);

			sem_wait(&shared->elf_sesh);
			printf("Elf: %d entering shop and making presence known\n", id);
			sem_post(&shared->seat);
			sem_wait(&shared->permission);

			printf("Elf :%d here is my problem...\n", id);
			sem_post(&shared->speaking);

			sem_wait(&shared->solution);
			printf("Elf: %d I am satisfied with that solution\n", id);
			printf("Elf: %d Leaving Santa's shop\n", id);


			sem_wait(&shared->mutex);
			--shared->elf_workshop;
			if (shared->elf_workshop == 1)
			{
				printf("Elf: %d you can go back to sleep now Santa!\n", id);
				sem_post(&shared->go_sleep);
			}
			sem_post(&shared->mutex);

		}
		else
		{
			++toy_number;
			--num_toys;
		}

	}
	sem_wait(&shared->mutex);
	--shared->elves_working;
	printf("Elf: %d terminated elves still working: %d\n",id, shared->elves_working);
	sem_post(&shared->mutex);

}
