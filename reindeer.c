// Aboutabl CS 450 Extra Credit: Santa and the Reindeers
// Author: Cole Inman
// Date: 11/14/2015

#include "mrsclaus.h"

int main(int argc, char *argv[])
{
	int shmid;
	key_t shmkey;
	int shmflg;
	shared_data *shared;
	int id = atoi(argv[2]);
	int yrs = atoi(argv[1]);

	shmkey = SHMKEY;
	shmflg = IPC_CREAT | S_IRUSR | S_IWUSR;
	shmid = shmget( shmkey, SHMEM_SIZE, shmflg);
	if (shmid != -1)
	{
		printf("Reindeer process id: %d started with shared memory id: %d\n", id, shmid);
	}

	shared = (shared_data *) shmat(shmid, NULL, 0);
	srandom(getpid());
	printf("Years: %d\n", yrs);
	while (yrs > 0)
	{
		yrs = shared->years;
		printf("Reindeer: %d Years left: %d\n", id, yrs);
		int sl = (random() % 6) + 3;
		printf("Reindeer: %d going to the tropics for %d seconds\n", id, sl);
		sleep(sl);

		printf("Reindeer: %d going to the North Pole\n", id);

		sem_wait(&shared->deer);
		++shared->deer_count;

		if (shared->deer_count < NUM_DEER)
		{
			sem_post(&shared->deer);
			printf("Reindeer: %d waiting in the hut\n", id);
			sem_wait(&shared->hut);
		}
		else
		{
			sem_post(&shared->deer);
			printf("Reindeer: %d waking those in the hut\n", id);
			for (int i = 1; i < NUM_DEER; i++)
				sem_post(&shared->hut);

			printf("Reindeer: %d waking up Santa\n", id);
			sem_post(&shared->santa_wake);
			sem_wait(&shared->santa_out);
		}

		printf("Reindeer: %d waiting for Santa's HoHoHo\n", id);
		sem_wait(&shared->hohoho);

		printf("Reindeer: %d flying with Santa\n", id);
		sem_wait(&shared->christmas);

		printf("Reindeer: %d came back from Christmas\n", id);
		sem_wait(&shared->deer);
		--shared->deer_count;
		sem_post(&shared->deer);
		sem_post(&shared->back);

		printf("Reindeer: %d can I be free?\n", id);
		sem_wait(&shared->free);

	}
}
