all: 
	gcc mrsclaus.c -std=c99 -w -pthread -o mrsclaus
	gcc reindeer.c -std=c99 -w -pthread -o reindeer
	gcc santa.c -std=c99 -w -pthread -o santa
	gcc elf.c -std=c99 -w -pthread -o elf
clean:
	rm mrsclaus
	rm reindeer
	rm santa	
	rm elf
